import torch
import numpy as np
import random
import scipy.sparse as sp
import torch.nn.functional as F
from models import GCN
import utils
from subgraph_enumerate import subgraph_enumerate

class gnn_counterfactual():
    def __init__(self,graph,max_depth,max_node,idx_map,features,labels,gnn_model):
        self.max_depth = max_depth
        self.max_node = max_node
        self.idx_map = idx_map
        self.features = features
        self.labels = labels
        self.model = gnn_model
        self.feature_lens = len(features[0])
        if isinstance(graph, list):
            self.init_graph_edg(graph=graph)
        elif isinstance(graph, dict):
            self.init_graph_adj(graph=graph)

    def init_graph_edg(self, graph):
        self.graph = {}
        for line in graph:
            p1_name,p2_name = line
            if p1_name == p2_name:
                continue
            if p1_name not in self.graph:
                self.graph[p1_name] = []
            if p2_name not in self.graph:
                self.graph[p2_name] = []
            if p2_name not in self.graph[p1_name]:
                self.graph[p1_name].append(p2_name)
            if p1_name not in self.graph[p2_name]:
                self.graph[p2_name].append(p1_name)

    def init_graph_adj(self, graph):
        self.graph = graph



    def get_sub_adj(self,subgraph):
        nodes_list = []
        for edge in subgraph:
            nodes_list.extend(edge)
        nodes_list = list(set(nodes_list))
        index = [self.idx_map[nodes] for nodes in nodes_list]
        labels_onehot = [int(k.numpy()) for k in self.labels[index]]
        sub_labels = np.zeros((len(index), self.labels.max()+1))
        for idx, k in enumerate(labels_onehot):
            sub_labels[idx][k] = 1
        sub_idx_map = {j:i for i, j in enumerate(nodes_list)}
        sub_features = self.features[index]
        sub_edge = np.array([[sub_idx_map[i[0]], sub_idx_map[i[1]]] for i in subgraph])
        sub_adj = utils.adj_matrix(sub_edge, sub_labels)
        sub_adj = utils.normalize(sub_adj)
        sub_adj = sp.csr_matrix(sub_adj)
        sub_adj = utils.sparse_mx_to_torch_sparse_tensor(sub_adj)
        return sub_adj,sub_features,sub_idx_map,sub_labels


    def get_model_output(self,sub_adj,sub_feature):
        output,attentions = self.model(sub_feature,sub_adj)
        output = F.softmax(output,dim=1)
        return output

    def get_connected_subgraph(self,subgraph):
        node_num,nodes,sub = len(subgraph),[subgraph[0][0]],[subgraph[0]]
        for i in range(1,node_num):
            edge = subgraph[i]
            if edge[0] in nodes or edge[1] in nodes:
                sub.append(subgraph[i])
                nodes.extend(edge)
        return sub

    def get_all_subgraph(self,subgraph):
        nodes,subgraphs,lens = [],[],len(subgraph)
        for i in range(1,lens):
            nodes.append(subgraph[i][1] if subgraph[i][1] not in nodes else subgraph[i][0])
            subgraphs.append(self.get_connected_subgraph(subgraph[0:i]+subgraph[i+1:]))
        return nodes,subgraphs

    def calculate_simulation(self,target_node,target_prediction,enumerated_subgraphs):
        self.simulation,self.subgraph_outputs = {},{}
        for subgraph in enumerated_subgraphs:
            sub_adj,sub_features,sub_idx_map,sub_labels = self.get_sub_adj(list(subgraph))
            sub_output = self.get_model_output(sub_adj,sub_features)
            simulate_kl = utils.sum_KL(target_prediction,sub_output[sub_idx_map[target_node]].detach().numpy())
            self.subgraph_outputs[subgraph] = sub_output[sub_idx_map[target_node]].detach().numpy()
            if len(subgraph) > 1:
                self.simulation[subgraph] = simulate_kl


    def calculate_subgraph_counterfactual_relevance(self,subgraph,target_prediction):
        nodes,subs = self.get_all_subgraph(list(subgraph))
        relevances = []
        if subgraph not in self.subgraph_outputs:
            sub_adj,sub_features,sub_idx_map,sub_labels = self.get_sub_adj(list(subgraph))
            sub_output = self.get_model_output(sub_adj,sub_features)
            self.subgraph_outputs[subgraph] = sub_output[sub_idx_map[target_node]].detach().numpy()
        for sub in subs:
            if tuple(sub) not in self.subgraph_outputs:
                sub_adj,sub_features,sub_idx_map,sub_labels = self.get_sub_adj(list(sub))
                sub_output = self.get_model_output(sub_adj,sub_features)
                self.subgraph_outputs[tuple(sub)] = sub_output[sub_idx_map[target_node]].detach().numpy()
            kl = utils.sum_KL(target_prediction,self.subgraph_outputs[tuple(sub)])-utils.sum_KL(target_prediction,self.subgraph_outputs[subgraph])
            number = len(subgraph) - len(sub)
            relevances.append(kl/number)
        return nodes,relevances,[abs(relevance) for relevance in relevances]

    # Calculate the simulability and counterfactual correlation of the two indicators used for each subgraph
    def calculate_counterfactual_relevance(self,target_prediction,enumerated_subgraphs):
        self.counterfactual_relevance = {}
        self.counterfactual_node = {}
        self.shapley_values = {}
        self.shapley_subgraph = {}
        for subgraph in enumerated_subgraphs:
            if len(subgraph) == 1:
                continue
            nodes,relevances,abs_relevances = self.calculate_subgraph_counterfactual_relevance(subgraph,target_prediction)
            for node,relevance in zip(nodes,relevances):
                if node not in self.shapley_values:
                    self.shapley_values[node] = []
                    self.shapley_subgraph[node] = {}
                self.shapley_values[node].append(relevance)
                self.shapley_subgraph[node][subgraph] = relevance
            max_relevance = max(abs_relevances)
            max_relevance_node = nodes[abs_relevances.index(max_relevance)]
            self.counterfactual_relevance[subgraph] = max_relevance
            self.counterfactual_node[subgraph] = max_relevance_node

    def get_subgraph_nodes(self, subgraph):
        nodes = []
        for edge in subgraph:
            nodes.extend(edge)
        return list(set(nodes))

    def get_sub_candidates(self,subgraph,max_depth_subgraph):
        nodes = self.get_subgraph_nodes(subgraph)
        candidates = []
        for node in nodes:
            for n in max_depth_subgraph[node]:
                if n not in nodes:
                    candidates.append((node,n))
        return candidates


    def get_shapley_explanation(self,max_depth_subgraph):
        shapley_values = {node:np.mean(self.shapley_values[node]) for node in self.shapley_values}
        sorted_shapley_values = sorted(shapley_values.items(),key=lambda x:x[1],reverse=True)
        best_node = sorted_shapley_values[0][0]
        shapley_subgraphs = self.shapley_subgraph[best_node]
        shapley_subgraphs = sorted(shapley_subgraphs.items(),key=lambda x:x[1],reverse=True)
        best_shapley_subgraphs,best_shapley_counterfactual = shapley_subgraphs[0]
        best_shapley_sim = self.simulation[best_shapley_subgraphs]
        best_shapley_counterfactual = abs(best_shapley_counterfactual)
        shapley_importances = {}
        for node in max_depth_subgraph:
            for n in max_depth_subgraph[node]:
                if n in shapley_values:
                    shapley_importances[(node,n)] = shapley_values[n]
                else:
                    shapley_importances[(node,n)] = 0
        return best_shapley_subgraphs,best_shapley_sim,best_shapley_counterfactual,shapley_importances
    def fast_non_dominated_sort(self,simulation,counterfactual_relevance):
        subgraphs = list(simulation.keys())
        lens = len(subgraphs)
        dominated_numbers = {sub:0 for sub in subgraphs}
        for i in range(lens):
            for j in range(i+1,lens):
                sub1,sub2 = subgraphs[i],subgraphs[j]
                if simulation[sub1] < simulation[sub2] and counterfactual_relevance[sub1] > counterfactual_relevance[sub2]:
                    dominated_numbers[sub2] += 1
                elif simulation[sub1] > simulation[sub2] and counterfactual_relevance[sub1] < counterfactual_relevance[sub2]:
                    dominated_numbers[sub1] += 1
        pareto_fronts,subgraph_simulation,subgraph_relevance = [],[],[]
        for sub in subgraphs:
            if dominated_numbers[sub] == 0:
                pareto_fronts.append(sub)
                subgraph_simulation.append((sub,simulation[sub]))
                subgraph_relevance.append((sub,counterfactual_relevance[sub]))
        subgraph_simulation = sorted(subgraph_simulation,key=lambda x:x[1])
        subgraph_relevance = sorted(subgraph_relevance,key=lambda x:x[1],reverse=True)
        simulate_order = {line[0]:index for index,line in enumerate(subgraph_simulation)}
        relevance_order = {line[0]:index for index,line in enumerate(subgraph_relevance)}
        balance_order = {sub:abs(simulate_order[sub]-relevance_order[sub]) for sub in pareto_fronts}
        balance_order = sorted(balance_order.items(),key=lambda x:x[1])
        pareto_results = [d[0] for d in balance_order if d[1] <= 2]
        if len(pareto_results) == 0:
            pareto_results = [balance_order[0][0]]
        return pareto_results,pareto_fronts

    def joint_sort(self,simulation,counterfactual_relevance):
        subgraph_simulation = sorted(simulation.items(),key=lambda x:x[1])
        subgraph_relevance = sorted(counterfactual_relevance.items(),key=lambda x:x[1],reverse=True)
        simulate_order = {line[0]:index for index,line in enumerate(subgraph_simulation)}
        relevance_order = {line[0]:index for index,line in enumerate(subgraph_relevance)}
        preference_order = {}
        subgraphs = list(simulation.keys())
        for sub in subgraphs:
            preference_order[sub] = simulate_order[sub] + relevance_order[sub]
        preference_results = sorted(preference_order.items(),key=lambda x:x[1])
        return preference_results[0][0]

    def get_counterfactual_explanation(self,target_node,target_prediction,enumerated_subgraphs):
        self.calculate_simulation(target_node,target_prediction,enumerated_subgraphs)
        self.calculate_counterfactual_relevance(target_prediction,enumerated_subgraphs)
        pareto_results, pareto_fronts = self.fast_non_dominated_sort(self.simulation, self.counterfactual_relevance)
        best_subgraph = self.joint_sort(self.simulation,self.counterfactual_relevance)
        return best_subgraph,pareto_results,pareto_fronts,self.subgraph_outputs,self.simulation,self.counterfactual_relevance


if __name__ == '__main__':
    dataset_name = 'cora' #'citeseer','pubmed','facebook','git'
    data_prefix = 'data/'

    # data_prefix = 'data/amazon_electronics_computers.npz'
    # dataset_name='amazon_cs'
    # data_prefix = 'data/amazon_electronics_photo.npz'
    # dataset_name = 'amazon_ph'
    # data_prefix = 'data/ms_academic_cs.npz'
    # dataset_name = 'academic_cs'
    # data_prefix = 'data/ms_academic_phy.npz'
    # dataset_name = 'academic_phy'

    print('loading data')
    graph, adj,normalized_adj, features, labels, nodes, idx_map, idx_train, idx_val, idx_test = utils.load_data(data_prefix,dataset_name)
    save_path = 'model/' + dataset_name + '_gcn_model.ph'

    nodes = [str(node) for node in nodes]
    max_depth = 2
    max_node = 4

    hidden_size = 16
    dropout_rate = 0.5
    lr_rate = 0.01
    weight_decay = 5e-4
    is_bias = True
    epochs = 200
    np.random.seed(42)
    torch.manual_seed(42)

    model = GCN(nfeat=features.shape[1],
            nhid= hidden_size,
            nclass=labels.max().item() + 1,
            dropout= dropout_rate,
            bias=is_bias)
    model.load_state_dict(torch.load(save_path))
    model.eval()
    target_output,attentions = model(features, normalized_adj)
    target_output = F.softmax(target_output,dim=1)

    subgraph_enum = subgraph_enumerate(graph,max_depth,max_node)
    gnn_counter = gnn_counterfactual(graph,max_depth,max_node,idx_map,features,labels,model)

    print('start counterfactual')
    counterfactual_result = [[],[]]
    count = 0

    for node_idx in idx_test[2:4]:
        target_node = nodes[int(node_idx)]
        if target_node not in subgraph_enum.graph:
            continue
        print(target_node,'start')
        enumerated_subgraphs,node_depth,node_order,max_depth_subgraph = subgraph_enum.enumerate_subgraph(target_node)
        print(target_node,len(enumerated_subgraphs))

        best_subgraph,pareto_results,pareto_fronts,subgraph_outputs,simulation,counterfactual_relevance = gnn_counter.get_counterfactual_explanation(target_node,target_output[node_idx].detach().numpy(),enumerated_subgraphs)
        shapley_subgraph,shapley_sim,shapley_counterfactual,shapley_importances = gnn_counter.get_shapley_explanation(max_depth_subgraph)

        count += 1
        best_sim = simulation[best_subgraph]
        best_coun = counterfactual_relevance[best_subgraph]
        counterfactual_result[0].append(best_sim)
        counterfactual_result[1].append(best_coun)
        print('best sim',best_sim,'best count',best_coun)

    print('counterfactual result',len(counterfactual_result[0]),np.mean(counterfactual_result[0]),np.mean(counterfactual_result[1]))
