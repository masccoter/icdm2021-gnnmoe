#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
Source Code for "Multi-objective Explanations of GNN Predictions", accepted to ICDM2021.
Paper Link: 
    https://ieeexplore.ieee.org/abstract/document/9679172
'''


'''
Replicating the paper's results.

1. Train the target GCN and/or GAT on a specific dataset.
    This model will be used as target model in the explanation task. 
    There are two ways to get the target model:
        (a) run the following:
            python train.py
        (b) or you can find the well trained target model on the "./model/" filefold.

2. Explaining a target model.
    To get the explanations as the paper, run the following:
        python gnn_counterfactual.py
    One can change the dataset or the samples to be explained in the corresponding lines.
'''




'''
Details of provided files and codes.

# Filefold:
    
- ./data/: This filefold contains original datasets.
           We offer all the datasets mentioned in the paper. 
           Including: 
               Citation networks: cora, citeseer, pubmed;
               Social networks: Musae-Facebook, Musae-Github
               Amazon-Computer, Amazon-Photo
               Coauthor-Computer, Coauthor-Physics

- ./model/: This filefold contains the target model (GCN and/or GAT) for all datasets mentioned above.



# Source Codes:
    
- models.py:
    It contains the implementation of the target model, e.g., GCN and/or GAT.
        
- train.py:
    It contains the training and test phases of the target model.
    The target model is trained and will be saved as long as the accuracy does not increase within a few epochs.

- subgraph_enumerate.py:
    It enumerates all the subgraphs satisfying the constrains by DFS. 
    (E.g., the number of nodes in the subgraphs is smaller than or equal to max_node,
           and all the node in the subgraphs is within max_depth-hop of the target node.)
    More details can be found in the paper and/or the source code.

- gnn_counterfactual.py:
    To obtain the explanation for the target node, it will:
        a) enumerate all the subgraphs for the target node by calling subgraph_enumerate.
        b) evaluate each subgraph w.r.t. two metrics (simulation and counterfactual relevance)
        c) pick the one on the Pareto Front based on the evaluation.
        d) * We all provides the implementation of the runner-up baseline, Shapley.
    More details can be found in the paper and/or the source code.

- utils.py:
    It contains the necessary auxiliary functions used in the project.
'''


'''
    Contacts: Chao Chen @ Lehigh University (chc517@lehigh.edu)
    Paper Link: https://ieeexplore.ieee.org/abstract/document/9679172
    Citation:
    @inproceedings{liu2021multi,
     title={Multi-objective Explanations of GNN Predictions},
     author={Liu, Yifei and Chen, Chao and Liu, Yazheng and Zhang, Xi and Xie, Sihong},
     booktitle={2021 IEEE International Conference on Data Mining (ICDM)},
     pages={409--418},
     year={2021},
     organization={IEEE}
}
'''



