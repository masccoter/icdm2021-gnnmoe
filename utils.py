import numpy as np
import scipy.sparse as sp
import torch
import csv
import json


def read_file(file_name):
    file_read=[]
    f = open(file_name , 'r')
    for line in f:
        file_read.append(line)

    clean_list = []
    for line in file_read:
        clean_list.append(line.split())
    return np.array(clean_list, dtype = 'str')

# one-hot
def encode_onehot(labels):
    classes = sorted(list(set(labels)))
    classes_dict = {c: np.identity(len(classes))[i, :] for i, c in
                    enumerate(classes)}
    labels_onehot = np.array(list(map(classes_dict.get, labels)),
                             dtype=np.int32)
    return labels_onehot


def normalize(mx):
    """Row-normalize sparse matrix"""
    rowsum = np.array(mx.sum(1))
    r_inv = np.power(rowsum, -1).flatten()
    r_inv[np.isinf(r_inv)] = 0.
    r_mat_inv = sp.diags(r_inv)
    mx = r_mat_inv.dot(mx)
    return mx


def sparse_mx_to_torch_sparse_tensor(sparse_mx):
    """Convert a scipy sparse matrix to a torch sparse tensor."""
    sparse_mx = sparse_mx.tocoo().astype(np.float32)
    indices = torch.from_numpy(
        np.vstack((sparse_mx.row, sparse_mx.col)).astype(np.int64))
    values = torch.from_numpy(sparse_mx.data)
    shape = torch.Size(sparse_mx.shape)
    return torch.sparse.FloatTensor(indices, values, shape)

def extract_pubmed_text(data_prefix):
    a = []
    myFile = open(data_prefix + 'Pubmed-Diabetes.DIRECTED.cites.tab' , 'r' )
    for Row in myFile:
        a.append(Row.split('\t'))
    myFile.close()

    a = a[2:]    # first two lines are descriptions
    paper_pair = []

    for idx in a:
        p1 = idx[1][6:]    # form: 'paper:' + id
        p2 = idx[3][6:-1]  # form: 'paper:' + id + '\n'
        paper_pair.append([p1, p2])

    b = []
    myFile= open(data_prefix + 'Pubmed-Diabetes.NODE.paper.tab', 'r' )
    for Row in myFile:
        b.append(Row.split('\t'))
    myFile.close()

    description = b[1]
    b = b[2:]       # first two lines are descriptions
    feature_list = []
    description = description[1:-1]
    for k in description:
        k = k[8:]
        stop = k.index(':')
        k = k[:stop]
        feature_list.append(k)

    nodes = []
    labels = np.zeros(len(b))
    features = np.zeros([len(b),len(description)])
    for idx, content in enumerate(b):
        nodes.append(content[0])
        labels[idx] = content[1][-1]
        feature_description = content[2:-1]
        for k in feature_description:
            start = k.index('=')
            feature_name = k[:start]
            feature_value = float( k[start+1 : ] )
            feature_location = feature_list.index(feature_name)
            features[idx][feature_location] = feature_value

    return np.array(paper_pair), features, encode_onehot(labels), np.array(nodes)

def remove_nodes(nodes, citation):
    new_citation = []
    for k in citation:
        p1 = k[0]
        p2 = k[1]
        if p1 in nodes and p2 in nodes:
            new_citation.append(k)
    return np.array(new_citation)


def adj_matrix(edges, labels_onehot):
    adj = sp.coo_matrix((np.ones(edges.shape[0]), (edges[:, 0], edges[:, 1])),
                        shape=(labels_onehot.shape[0], labels_onehot.shape[0]),
                        dtype=np.float32)
    adj = adj + adj.T.multiply(adj.T > adj) - adj.multiply(adj.T > adj)
    return adj

#citeseer,cora,pubmed
def load_data_citation(data_prefix,dataset_name):
    if dataset_name == 'cora' or dataset_name == 'citeseer':
        citation = read_file(data_prefix + dataset_name + '.cites')
        contents = read_file(data_prefix + dataset_name + '.content')
        features = sp.csr_matrix(contents[:, 1:-1], dtype=np.float32)
        labels = contents[:,-1]
        labels_onehot = encode_onehot(labels)
#        labels_onehot = encode_onehot(contents[:,-1])
        nodes = contents[:,0]
    elif dataset_name == 'pubmed':
        citation, contents, labels_onehot, nodes = extract_pubmed_text(data_prefix)
        features = sp.csr_matrix(contents, dtype=np.float32)
    # remove if nodes in edges without features
    citation = remove_nodes(nodes, citation)
    # map node/citation index into int-space
    idx_map = {j: i for i, j in enumerate(nodes)}
    graph = [[str(i[0]), str(i[1])] for i in citation]
    edges = np.array([[idx_map[i[0]], idx_map[i[1]]] for i in citation])
#    adj = sp.csr_matrix(adj_matrix(edges, labels_onehot))
    adj = adj_matrix(edges, labels_onehot)
    normalized_adj = normalize(adj + sp.eye(adj.shape[0]))
    normalized_adj = sp.csr_matrix(normalized_adj)

    features = normalize(features)
    features = torch.FloatTensor(np.array(features.todense()))
    labels_onehot = torch.LongTensor(np.where(labels_onehot)[1])
    normalized_adj = sparse_mx_to_torch_sparse_tensor(normalized_adj)
    #adj = sparse_mx_to_torch_sparse_tensor(adj)

    train_ratio = 0.5
    val_ratio = 0.2
    test_ratio = 1 - train_ratio - val_ratio
    idx_train = torch.LongTensor(range( int(train_ratio * len(nodes)) ) )
    idx_val = torch.LongTensor(range( int(train_ratio * len(nodes) + 1), int((train_ratio+val_ratio)*len(nodes)) ) )
    idx_test = torch.LongTensor(range( int( (1-test_ratio) * len(nodes) + 1), len(nodes) ) )

    return graph, adj,normalized_adj, features, labels_onehot, nodes, idx_map, idx_train, idx_val, idx_test


def read_csv(file_name):
    data = []
    csv_reader = csv.reader(open(file_name))
    for row in csv_reader:
        data.append(row)
    return data

def get_features_nums(features):
    feature_sets = set()
    for node in features:
        for feature in features[node]:
            feature_sets.add(feature)
    return len(feature_sets)

def get_feature_vector(features,feature_nums):
    nodes = len(features)
    feature_vectors = []
    for i in range(nodes):
        vector = np.zeros((feature_nums),dtype=np.float32)
        for n in features[str(i)]:
            vector[int(n)] = 1
        feature_vectors.append(vector)
    features_vectors = sp.csr_matrix(feature_vectors, dtype=np.float32)
    return features_vectors


#github and facebook
def load_data_musae(data_prefix,dataset_name):
    edges_data = read_csv(data_prefix + 'musae_'+dataset_name+'_edges.csv')[1:]
    targets = read_csv(data_prefix + 'musae_'+ dataset_name+'_target.csv')[1:]
    with open(data_prefix+'musae_'+dataset_name+'_features.json','r') as f:
        features = json.load(f)

    nodes = list(features.keys())
    nodes = [str(n) for n in nodes]
    feature_nums = get_features_nums(features)
    features_vectors = get_feature_vector(features,feature_nums)
    labels = [target[-1] for target in targets]
    labels_onehot = encode_onehot(labels)

    idx_map = {j: i for i, j in enumerate(nodes)}
    graph = [[str(i[0]), str(i[1])] for i in edges_data]
    edges = np.array([[str(i[0]), str(i[1])] for i in edges_data])

    adj = adj_matrix(edges, labels_onehot)
    normalized_adj = normalize(adj + sp.eye(adj.shape[0]))
    normalized_adj = sp.csr_matrix(normalized_adj)

    features = normalize(features_vectors)
    features = torch.FloatTensor(np.array(features.todense()))
    labels_onehot = torch.LongTensor(np.where(labels_onehot)[1])
    normalized_adj = sparse_mx_to_torch_sparse_tensor(normalized_adj)

    train_ratio = 0.5
    val_ratio = 0.2
    test_ratio = 1 - train_ratio - val_ratio
    idx_train = torch.LongTensor(range( int(train_ratio * len(nodes)) ) )
    idx_val = torch.LongTensor(range( int(train_ratio * len(nodes) + 1), int((train_ratio+val_ratio)*len(nodes)) ) )
    idx_test = torch.LongTensor(range( int( (1-test_ratio) * len(nodes) + 1), len(nodes) ) )

    return graph, adj,normalized_adj, features, labels_onehot, nodes, idx_map, idx_train, idx_val, idx_test




def load_data_network(data_prefix):
    data = np.load(data_prefix,allow_pickle=True)
    labels = data['labels']
    class_names = data['class_names']
    adj = sp.csr_matrix((data['adj_data'], data['adj_indices'], data['adj_indptr']), shape=data['adj_shape'])
    features = sp.csr_matrix((data['attr_data'], data['attr_indices'],data['attr_indptr']), shape=data['attr_shape'])
    labels_onehot = encode_onehot(labels)
    graphs = adj.A

    features = normalize(features)
    features = torch.FloatTensor(np.array(features.todense()))
    labels_onehot = torch.LongTensor(np.where(labels_onehot)[1])

    adj = adj + adj.T.multiply(adj.T > adj) - adj.multiply(adj.T > adj)
    normalized_adj = normalize(adj + sp.eye(adj.shape[0]))
    normalized_adj = sparse_mx_to_torch_sparse_tensor(normalized_adj)

    nodes = [str(i) for i in range(len(labels))]
    idx_map = {j: i for i, j in enumerate(nodes)}
    graph = []
    for i in range(len(nodes)):
        link_nodes = np.where(graphs[i])[0]
        for n in link_nodes:
            graph.append([str(i),str(n)])

    train_ratio = 0.5
    val_ratio = 0.2
    test_ratio = 1 - train_ratio - val_ratio
    idx_train = torch.LongTensor(range( int(train_ratio * len(nodes)) ) )
    idx_val = torch.LongTensor(range( int(train_ratio * len(nodes) + 1), int((train_ratio+val_ratio)*len(nodes)) ) )
    idx_test = torch.LongTensor(range( int( (1-test_ratio) * len(nodes) + 1), len(nodes) ) )

    return graph, adj,normalized_adj, features, labels_onehot, nodes, idx_map, idx_train, idx_val, idx_test, class_names


def load_data(data_prefix,dataset_name):
    if dataset_name in ['cora','citeseer','pubmed']:
        graph, adj,normalized_adj, features, labels, nodes, idx_map, idx_train, idx_val, idx_test = load_data_citation(data_prefix,dataset_name)
    elif dataset_name in ['facebook','git']:
        graph, adj,normalized_adj, features, labels, nodes, idx_map, idx_train, idx_val, idx_test = load_data_musae(data_prefix,dataset_name)
    else:
        graph, adj,normalized_adj, features, labels, nodes, idx_map, idx_train, idx_val, idx_test, class_names = load_data_network(data_prefix)
    return graph, adj,normalized_adj, features, labels, nodes, idx_map, idx_train, idx_val, idx_test


def smooth(arr, eps = 1e-5):
    if 0 in arr:
        return abs(arr-eps)
    else:
        return arr

def KL_divergence(P, Q):
    # Input P and Q would be vector (like messages or priors)
    # Will calculate the KL-divergence D-KL(P || Q) = sum~i ( P(i) * log(Q(i)/P(i)) )
    # Refer to Wikipedia https://en.wikipedia.org/wiki/Kullback%E2%80%93Leibler_divergence
    P = np.array(P)
    Q = np.array(Q)
    P = smooth(P)
    Q = smooth(Q)
    return sum( P * np.log(P/Q))

def sum_KL(P,Q):
    return KL_divergence(P,Q) + KL_divergence(Q,P)

