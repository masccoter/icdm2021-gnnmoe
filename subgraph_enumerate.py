import numpy as np
import utils


class subgraph_enumerate():
    def __init__(self,graph,max_depth,max_node):
        self.max_depth = max_depth
        self.max_node = max_node
        if isinstance(graph, list):
            self.init_graph_edg(graph=graph)
        elif isinstance(graph, dict):
            self.init_graph_adj(graph=graph)

    def init_graph_edg(self, graph):
        self.graph = {}
        for line in graph:
            p1_name,p2_name = line
            if p1_name == p2_name:
                continue
            if p1_name not in self.graph:
                self.graph[p1_name] = []
            if p2_name not in self.graph:
                self.graph[p2_name] = []
            if p2_name not in self.graph[p1_name]:
                self.graph[p1_name].append(p2_name)
            if p1_name not in self.graph[p2_name]:
                self.graph[p2_name].append(p1_name)

    def init_graph_adj(self, graph):
        self.graph = graph

    def sort_neighbor_nodes(self,subgraph):
        nodes_degree,sorted_graph = {},{}
        for node in subgraph:
            nodes = [n for n in subgraph[node] if n in self.node_depth]
            sorted_graph[node],nodes_degree[node] = nodes,len(nodes)
        return {node:sorted(sorted_graph[node],key= lambda x:(self.node_depth[x],nodes_degree[x]),reverse=True) for node in sorted_graph}

    def get_max_depth_subgraph(self,start_node):
        subgraph = {start_node:self.graph[start_node]}
        self.node_depth,depth,nodes = {start_node:0},1,[start_node]
        while depth <= self.max_depth:
            neighbor_nodes = []
            for node in nodes:
                neighbor_nodes.extend([n for n in self.graph[node] if n not in subgraph])
            neighbor_nodes = list(set(neighbor_nodes))
            for node in neighbor_nodes:
                subgraph[node],self.node_depth[node] = self.graph[node],depth
            nodes,depth = neighbor_nodes,depth+1
        self.node_order,self.max_depth_subgraph = list(subgraph.keys()),self.sort_neighbor_nodes(subgraph)
        
    def sort_subgraph(self,subgraph):
        return tuple(sorted(subgraph,key=lambda x:(self.node_depth[x[0]],self.node_depth[x[1]],self.node_order.index(x[0]),self.node_order.index(x[1]))))

    def get_subgraph_nodes(self, subgraph):
        nodes = []
        for edge in subgraph:
            nodes.extend(edge)
        return list(set(nodes))

    def get_node_candidate_edges(self, subgraph):
        candidate_edges = [(subgraph[-1][-1], n) for n in self.max_depth_subgraph[subgraph[-1][-1]] if self.node_depth[n] > self.node_depth[subgraph[-1][-1]]]
        return list(set(candidate_edges))

    def get_sub_candidate_edges(self, subgraph,forbidden_edges):
        nodes,candidate_edges = self.get_subgraph_nodes(subgraph),[]
        for node in nodes:
            candidate_edges.extend([(node, n) for n in self.max_depth_subgraph[node] if n not in nodes and (node,n) not in forbidden_edges and (n,node) not in forbidden_edges])
        return list(set(candidate_edges))

    def get_candidate_edges(self,subgraph,forbidden_edges,enumerate_type):
        return self.get_node_candidate_edges(subgraph) if enumerate_type == 'node' else self.get_sub_candidate_edges(subgraph,forbidden_edges)

    # DFS traverse enumerates all subgraphs.
    # There are two mode types: node means depth-first traversal, finding the edge with the largest depth,
    # subgraph means entering the expend mode and completing the subgraph to the maximum number of nodes
    def dfs_expend(self,subgraph,forbiddens,enumerate_type):
        subgraph_nodes,sorted_subgraph = len(subgraph),self.sort_subgraph(subgraph)
        if sorted_subgraph not in self.enumerate_result:
            self.enumerate_result[sorted_subgraph] = 1
        if subgraph_nodes == self.max_node:
            return
        candidates = self.get_candidate_edges(subgraph,forbiddens,enumerate_type)
        if len(candidates) == 0:
            return
        forbidden = []
        for edge in candidates:
            forbidden.append(edge)
            self.dfs_expend(subgraph +[edge], forbidden,enumerate_type)
        return

    # DFS traverse enumerates all subgraphs.
    # There are two mode types: node means depth-first traversal, finding the edge with the largest depth,
    # subgraph means entering the expend mode and completing the subgraph to the maximum number of nodes
    def dfs_enumerate(self,subgraph,forbiddens,enumerate_type):
        subgraph_nodes = len(subgraph)
        sorted_subgraph = self.sort_subgraph(subgraph)
        if sorted_subgraph not in self.enumerate_result:
            self.enumerate_result[sorted_subgraph] = 1
        if subgraph_nodes == self.max_node:
            return
        candidates = self.get_candidate_edges(subgraph,forbiddens,enumerate_type)
        forbidden = []
        for edge in candidates:
            forbidden.append(edge)
            self.dfs_enumerate(subgraph +[edge], forbidden,enumerate_type)
        self.dfs_expend(subgraph,forbidden,'subgraph')
        return

    def enumerate_subgraph(self,start_node):
        self.get_max_depth_subgraph(start_node)
        self.enumerate_result = {}
        self.dfs_enumerate([(start_node,start_node)],[],'node')
        return self.enumerate_result,self.node_depth,self.node_order,self.max_depth_subgraph


if __name__ == '__main__':
    # test case
    dataset_name = 'cora'
    data_prefix = 'data/'

    print('loading data')
    graph, adj,normalized_adj, features, labels, nodes, idx_map, idx_train, idx_val, idx_test = utils.load_data(data_prefix,dataset_name)
    nodes = [str(node) for node in nodes]
    print('total nodes',len(nodes))

    max_depth = 2
    max_node = 4
    np.random.seed(42)
    subgraph_enum = subgraph_enumerate(graph,max_depth,max_node)

    # for node_idx in idx_test[11:1000]:
    for node_idx in idx_test[11:12]:
        target_node = nodes[int(node_idx)]
        if target_node not in subgraph_enum.graph:
            continue
        enumerated_result,node_depth,node_order,max_depth_subgraph = subgraph_enum.enumerate_subgraph(target_node)
        print('target node',target_node,len(enumerated_result))

