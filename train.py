import numpy as np
import torch
import torch.nn.functional as F
import torch.optim as optim
import utils
from models import GCN
from sklearn.metrics import f1_score


def accuracy(output, labels):
    preds = output.max(1)[1].type_as(labels)
    #print('pred',preds)
    correct = preds.eq(labels).double()
    #print('label',correct)
    correct = correct.sum()
    return correct / len(labels)


def f1(output, labels):
    preds = output.max(1)[1]
    preds = preds.cpu().detach().numpy()
    labels = labels.cpu().detach().numpy()
    micro = f1_score(labels, preds, average='micro')
    macro = f1_score(labels, preds, average='macro')
    return micro, macro


def train_model(epoch):
    model.train()
    optimizer.zero_grad()
    output,adjs = model(features, normalized_adj)
    output = F.log_softmax(output, dim=1)
    #print('output',output)
    loss_train = F.nll_loss(output[idx_train], labels[idx_train])
    acc_train = accuracy(output[idx_train], labels[idx_train])
    loss_train.backward()
    optimizer.step()
    loss_val = F.nll_loss(output[idx_val], labels[idx_val])
    acc_val = accuracy(output[idx_val], labels[idx_val])
    print('Epoch: {:04d}'.format(epoch+1),
          'loss_train: {:.4f}'.format(loss_train.item()),
          'acc_train: {:.4f}'.format(acc_train.item()),
          'loss_val: {:.4f}'.format(loss_val.item()),
          'acc_val: {:.4f}'.format(acc_val.item()))


def model_test():
    model.eval()
    output,adjs = model(features, normalized_adj)
    output = F.log_softmax(output, dim=1)
    loss_test = F.nll_loss(output[idx_test], labels[idx_test])
    acc_test = accuracy(output[idx_test], labels[idx_test])
    print("Test set results:",
          "loss= {:.4f}".format(loss_test.item()),
          "accuracy= {:.4f}".format(acc_test.item()))
    return acc_test.item()


if __name__ == '__main__':
    dataset_name = 'cora'
    data_prefix = 'data/'
    model_name = 'gcn'
    
    
    hidden_size = 16
    dropout_rate = 0.5
    lr_rate = 0.1
    weight_decay = 5e-4
    is_bias = True
    epochs = 500
    np.random.seed(42)
    torch.manual_seed(42)
    save_path = 'data/' + dataset_name + '_' + model_name + '_model.ph'
    
    
    print('loading data')
    graph, adj,normalized_adj, features, labels, nodes, idx_map, idx_train, idx_val, idx_test = utils.load_data(data_prefix,dataset_name)
    print('edges',len(graph),'nodes',len(nodes))
    print('features',features.shape[1],'label',labels[0])
    print('train nodes',len(idx_train),'val nodes',len(idx_val),'test nodes',len(idx_test))
    
    model = GCN(nfeat=features.shape[1],nhid= hidden_size,nclass=labels.max().item() + 1,
                dropout= dropout_rate,bias=is_bias)
    optimizer = optim.Adam(model.parameters(),
                           lr=lr_rate, weight_decay=weight_decay)
    
    best_acc = 0
    early_stop_step = 10
    temp_early_stop_step = 0
    
    print('start training')
    for epoch in range(epochs):
        train_model(epoch)
        if epoch % 10 == 0:
            temp_acc = model_test()
            print('save model')
            if temp_acc > best_acc:
                best_acc = temp_acc
                torch.save(model.state_dict(), save_path)
                temp_early_stop_step = 0
            else:
                temp_early_stop_step += 1
                if temp_early_stop_step >= early_stop_step:
                    print('early stop')
                    break

